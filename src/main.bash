#! /bin/bash



function get(){
	if [[ -d /home/$USER/baur/ ]];then
		echo "downloading sources"
	else 
		mkdir /home/$USER/baur/
	fi
	cd /home/$USER/baur/
	nb=$(echo $@ | wc -w)
	nbs=$(expr $nb - 1 )
	if [[ $nbs == 1 ]];then
		rm -rf ~/baur/$2
		echo $1
		git clone https://aur.archlinux.org/$2
		echo "Downloading complete"
	else

		for i in $(seq 1 $nbs)
		do
			truc=$(expr $i + 1)
			to_get=$(echo "$@" | cut -d ' ' -f $truc)
			rm -rf ~/baur/$to_get
			echo $to_get
			git clone https://aur.archlinux.org/$to_get
			echo "Downloading complete"
		done
	fi
}

function install(){
	nb=$(echo $@ | wc -w)
	nbs=$(expr $nb - 1)
	if [[ $nbs == 1 ]];then
		cd /home/$USER/baur/$2
		read -p "Do you want to install $2 ? o/n : " installation
		if [[ $installation == "o" ]];then
			echo "installing the package"
			makepkg -i --noconfirm
		fi
	else 
		
		for i in $(seq 1 $nbs)
		do
			truc=$(expr $i + 1)
			arg=$(echo $@ | cut -d ' ' -f $truc)
			cd /home/$USER/baur/$arg
			read -p "Do you want to install $2 ? o/n : " installation
			if [[ $installation == "o" ]];then
				echo "installing the package"
				makepkg -i --noconfirm
			fi
		done
	fi
}

function build() {
	nb=$(echo $@ | wc -w)
	nbs=$(expr $nb - 1)
	if [[ $nbs == 1 ]];then
		cd /home/$USER/baur/$2
		pkgbuild=/home/$USER/baur/$2/PKGBUILD
		read -p "Do you want to review the pkgbuild ? o/n " review
		if [[ $review == "o" ]];then
			less $pkgbuild
		fi
		read -p "Do you want to edit the pkgbuild ? o/n : " edit
		if [[ $edit == "o" ]];then
			nano $pkgbuild
		fi
	else
		for i in $(seq 1 $nbs)
		do
			truc=$(expr $i + 1)
			arg=$(echo $@ | cut -d ' ' -f $truc)
			cd /home/$USER/baur/$arg
			pkgbuild=/home/$USER/baur/$arg/PKGBUILD
			read -p "Do you want to review the pkgbuild of $arg ? o/n " review
			if [[ $review == "o" ]];then
				less $pkgbuild
			fi
			read -p "Do you want to edit the pkgbuild of $arg ? o/n : " edit
			if [[ $edit == "o" ]];then
				nano $pkgbuild
			fi
		done
	fi
}

function verify_update() {
	nb=$(pacman -Qm | wc -l)
	for i in $(seq 1 $nb)
	do
		package=$(pacman -Qm | sed -n ${i}p | cut -d ' ' -f 1)
		version=$(pacman -Q ${package} | cut -d ' ' -f 2)
		get $package
		cd /home/$USER/baur/$package
		aurver=$(cat PKGBUILD | grep pkgver | head -n 1 | cut -d '=' -f 2)
		aurel=$(cat PKGBUILD | grep pkgrel | head -n 1 | cut -d '=' -f 2)
		to_verify="${aurver}-${aurel}"
		if [[ $to_verify == $version ]];then
			echo "You don't need to update"
		else 
			echo "You have to update some packages"
			read -p "Do you want to update $package ? o/n : " update_package
			if [[ $update_package == "o" ]];then
				cd /home/$USER/baur/$package
				build $package
				install $package
			fi

		fi
		
			
	done
}

function search() {
	url="https://aur.archlinux.org/packages/?O=0&K=${1}"
	firefox $url
}

function uninstall() {
	args=$(echo "${*:2}")
	sudo pacman -R $args
}

function main(){
	echo "Hi"

	if [[ $1 == "update" ]];then
		verify_update
	fi

	if [[ $1 == "install" ]];then
		get $@
		build $@
		install $@
	fi

	if [[ $1 == "search" ]];then	
		search $2
	fi

	if [[ $1 == "remove" ]];then
		uninstall $@
	fi
}
main "$@"
